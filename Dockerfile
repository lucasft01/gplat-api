FROM node:latest

ENV APP_ROOT /src

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}
COPY . ${APP_ROOT}
RUN npm install

RUN apt-get update && apt-get install -y build-essential && apt-get install -y python && apt-get install python3-pip -y
RUN pip3 install pandas xlrd
ENV DATABASE admin
ENV PORT 27017
ENV USERNAME root
ENV PASSWORD example
ENV HOST localhost

import express from 'express'
const app = express()
import gplast from './routes/gplast'
import cors from 'cors'
import bodyParser from 'body-parser'

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/gplast', gplast)

app.get('/*', (req, res) => res.send('ok'))

app.listen(3015, async () => {
  console.log('ok')
})

process.on('SIGINT', () => { console.log("Bye bye!"); process.exit(); });

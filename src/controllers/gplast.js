import runPython from '../helpers/runPython'

async function doCount(req, res) {
  try {
    const { planejamentoTroca, tempoPreventiva, tempoCorretiva, diasTrabalho } = req.body
    const path = './src/helpers/python'
    const scriptPython = 'modelo.py'
    const params = [planejamentoTroca, tempoPreventiva, tempoCorretiva, diasTrabalho]
    const resultPython = await runPython(path, scriptPython, params)
    console.log({resultPython})
    res.status(200).send( resultPython )
  } catch (e) {
    console.error(e)
    res.sendStatus(500)
  }
}

export { doCount }

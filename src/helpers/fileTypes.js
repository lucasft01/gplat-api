const filtros = {
  audio: {
    "audio/mp3": "mp3",
    "audio/wav": "wav",
    "audio/x-wav": "wav",
    "audio/ogg": "oga",
    "audio/aac": "aac",
    "audio/vnd.dlna.adts": "aac",
  },
  video: {
    "video/mp4": "mp4",
    "video/x-m4v": "mp4",
    "video/mpeg": "mpeg",
    "video/ogg": "ogv",
    "video/mov": "mov",
    "video/quicktime": "mov"
  },
  file: {
    "text/csv": "csv",
    "text/plain": "txt",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx"
  },
  imagensAlunos: {
    "image/jpeg": "jpeg",
    "image/png": "png",
  },
  fotoPerfil: {
    "image/jpeg": "jpeg",
    "image/png": "png",
  }
}

const getExtensao = (mimetype) => {
  let cont = false
  let keysFiltros = Object.keys(filtros)
  let extensaoEscolhida = []
  keysFiltros.map(key => {
    if (filtros[key][mimetype] && cont === false) {
      cont = true
      extensaoEscolhida.push(filtros[key][mimetype])
    }
  })

  return extensaoEscolhida[0]
}

const isMimetypePermitido = (tipo, mimetype) => {
  return (filtros[tipo] && filtros[tipo][mimetype])
}

export { getExtensao, isMimetypePermitido }

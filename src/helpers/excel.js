import runPython from './runPython'

async function readExcel(excelPath, excel) {
  const scriptPath = './src/helpers'
  const scriptPython = 'readExcel.py'
  const params = [excelPath, excel]
  const returnPython = await runPython(scriptPath, scriptPython, params)
  return returnPython
}

async function countLinesExcel(excelPath, excel) {
  const scriptPath = './src/helpers'
  const scriptPython = 'countLinesExcel.py'
  const params = [excelPath, excel]
  const countRows = await runPython(scriptPath, scriptPython, params)
  return countRows
}

async function getDataExcelInParts(excelPath, excel, indexSheet, beginLine, finishLine) {
  const scriptPath = './src/helpers'
  const scriptPython = 'readExcelInParts.py'
  const params = [excelPath, excel, indexSheet, beginLine, finishLine]
  const excelInParts = await runPython(scriptPath, scriptPython, params)
  return excelInParts
}

export { readExcel, countLinesExcel, getDataExcelInParts }

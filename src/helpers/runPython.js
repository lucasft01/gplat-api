import { PythonShell } from 'python-shell';

export default async function runPython(pathScriptPython, scriptPython, params) {
  const options = {
    // mode: 'json',
    pythonOptions: ['-u'],
    scriptPath: pathScriptPython,
    args: params
  };
  const runCommand = () => new Promise((resolve, reject) => {
    PythonShell.run(scriptPython, options, function (err, results) {
      if (err) reject(err)
      if (typeof (results) == 'object')
        results = results.map(result => JSON.parse(result))
      resolve(results)
    });
  })
  const returnPython = await runCommand()
  return returnPython
}

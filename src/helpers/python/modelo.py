import sys
import pandas as pd
from datetime import datetime

meses = {"1":["2018-01-01","2018-01-31"],"2":["2018-02-01","2018-01-28"],"3":["2018-03-01","2018-03-31"],
         "4":["2018-04-01","2018-01-30"],"5":["2018-05-01","2018-05-31"],"6":["2018-06-01","2018-06-30"],
         "7":["2018-07-01","2018-07-31"],"8":["2018-08-01","2018-08-31"],"9":["2018-09-01","2018-09-30"],
         "10":["2018-10-01","2018-10-31"],"11":["2018-11-01","2018-11-30"],"12":["2018-12-01","2018-12-31"]}

def verificaFormato(data):
   try:
       datetime.strptime(str(data),"%H:%M:%S")
       return True
   except ValueError:
       return False

def duracao(index,df):
   duracao = []
   try:
       i = index
       for element in df:
           duracao.append(element)
           i = i + 1
       return duracao
   except ValueError:
       return False

def totalCorretivaMes(serieDateTime):
    totalMinutos = 0
    totalHoras = 0
    totalTempoHoras = 0
    for h  in serieDateTime:
        totalHoras = totalHoras + h.hour
        totalMinutos = totalMinutos + h.minute
    totalTempoHoras = totalMinutos/60 + totalHoras
    return totalTempoHoras


def totalMeses(df,meses):
    i = 12
    totalMes = []
    while i > 0:
        mes  = df[(df['DATA'] > meses[str(i)][0]) & (df['DATA'] <  meses[str(i)][1])]
        total = duracao(0,mes["TOTAL"])
        totalMes.append(totalCorretivaMes(total))
        i = i - 1
    return totalMes


def quantidadeMes(lista):
    count = 0
    for item in lista:
        if item != 0:
            count = count + 1
    return count


def homemExtra(tempoCorretiva, planejamentoTroca, tempoPreventiva, mediaTroca, mediaCorretiva):
    diasTrabalho = 28
    horaDia = 6.33
    horaHomemMes = diasTrabalho*horaDia*15
    capacidadeEstimada = mediaCorretiva + mediaTroca
    tempoTroca = 7
    tempoTrocaPlanejada = planejamentoTroca*tempoTroca

    capacidadeSimulada = tempoCorretiva +  tempoTrocaPlanejada + tempoPreventiva
    if capacidadeEstimada > capacidadeSimulada:
        return 0
    else:
        result = round(abs((capacidadeSimulada - capacidadeEstimada))/horaHomemMes,2)
        return  result


df2 = pd.read_excel('./src/helpers/python/ManutencaoGeral.xlsx')
df2 = df2[df2["TIPO"] == 0]
df2 = df2[df2["TIPOMANUTENCAO"] == 0]
serie = df2["TOTAL"]

listaMes = totalMeses(df2,meses)
quantidadeMes = quantidadeMes(listaMes)
serie = pd.Series(listaMes).astype(float) #corrigir

lista = [214,442,399,724,1075]
series = pd.Series(lista).astype(float)


mediaCorretiva = series.mean()
stdCorretiva = series.std()
df = pd.read_excel('./src/helpers/python/ResumoAnalitico2.xlsx')
tempoTroca = 7
Somatorio  = df.sum()*tempoTroca
mediaTroca = Somatorio.mean()
stdTroca = Somatorio.std()

planejamentoTroca = int(sys.argv[1])
tempoPreventiva = float(sys.argv[2])
tempoCorretiva  = float(sys.argv[3])

resultado = []


pontoMedio = homemExtra(tempoCorretiva,planejamentoTroca,tempoPreventiva,mediaTroca,mediaCorretiva)
pontoSuperior = homemExtra(tempoCorretiva,planejamentoTroca,tempoPreventiva,mediaTroca + 2*stdTroca,mediaCorretiva + 2*stdCorretiva)
pontoInferior = homemExtra(tempoCorretiva,planejamentoTroca,tempoPreventiva,mediaTroca - 2*stdTroca,mediaCorretiva - 2*stdCorretiva)

resultado.append(pontoInferior)
resultado.append(pontoMedio)
resultado.append(pontoSuperior)

print(resultado)
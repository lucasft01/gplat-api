import fs from 'fs'
import { promisify } from 'util'

async function getFiles(path) {
  const readDirAsync = promisify(fs.readdir);
  const dir = await readDirAsync(path)
  return dir
}

async function getFile(fileName) {
  const readFileAsync = promisify(fs.readFile)
  const file = await readFileAsync(fileName, 'utf8')
  return file
}

async function getLastFileCreate(documentPath) {
  const files = await getFiles(documentPath)
  const array = await Promise.all(
    files.map(async function (file) {
      const statusFile = await getStatusFile(documentPath, file)
      const birthTime = statusFile.birthtime
      return { file, birthTime }
    })
  )
  const sortArray = array.sort(function (a, b) { return b.birthTime - a.birthTime })
  const lastCreatedFile = sortArray[0]
  return lastCreatedFile
}

async function getStatusFile(path, file) {
  const readStatAsync = promisify(fs.stat)
  const statusFile = await readStatAsync(`${path}/${file}`)
  return statusFile
}

async function deleteFile(path, file) {
  const unlinkFile = promisify(fs.unlink)
  await unlinkFile(`${path}/${file}`)
  return true
}

export { getFiles, getFile, getStatusFile, getLastFileCreate, deleteFile }
